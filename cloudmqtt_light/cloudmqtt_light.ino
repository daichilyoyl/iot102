/*
 *  cloudmqtt_light.ino
 *  Copyright (c) 2016 Masami Yamakawa
 *  setupWifi() and reconnect() functions are Copyright (c) 2008-2015 Nicholas O'Leary
 *  This software is released under the MIT License.
 *  http://opensource.org/licenses/mit-license.php
 * 
 *  An example to demonstrate analogRead and analogWrite over MQTT.
 *  cloudmqtt_led.ino subscriber
 *  cloudmqtt_light.ino publisher
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "MONOXIT";
const char* password = "??????";
const char* mqttServer = "???.cloudmqtt.com";
const char* mqttDeviceId = "WSPOC001";
const char* mqttUser = "???";
const char* mqttPassword = "???";
const char* mqttTopic = "school/light";
const int mqtt_port = 24326;
const int updateIntervalMillis = 1000;

//Connect through TLS1.1
WiFiClientSecure espClient;
PubSubClient client(espClient);
unsigned long lastUpdateMillis = 0;
unsigned int value = 0;

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  setupWifi();
  client.setServer(mqttServer, mqtt_port);
}

void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(mqttDeviceId, mqttUser, mqttPassword)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastUpdateMillis > updateIntervalMillis) {
    lastUpdateMillis = now;
    value=analogRead(A0);
    // payloadに{"myName":"WSPOC001","hikari":センサー値}をセット
    String payload = "{\"myName\":\"";
    payload += mqttDeviceId;
    payload += "\",\"hikari\":";
    payload += value;
    payload += "}";
    Serial.print("Publish message: ");
    Serial.println(payload);
    // payloadにセットされたJSON形式メッセージを投稿
    client.publish(mqttTopic, (char*) payload.c_str());
  }
}
